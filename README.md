# Spark programming 3 #

I'm learning for the [Hortonworks Spark Certification](https://hortonworks.com/services/training/certification/hdp-certified-spark-developer/) 
in these days. So let me share an interesting task with you.

## Task ##

You've got **two big data** files in the following format:

Employees (id,name,role):

	1,John,Programmer
	2,Mary,Director
	3,Dick,Assistant
	99,Wayne,Tester
	65,Jerry,QA
	4,Catherine,Receptionist


Travels (employee_id, city, country)

	1,NewYork,USA
	1,Dallas,USA
	3,Montreal,CAN
	3,NewYork,USA
	99,NewYork,USA
	3,Berlin,GER
	99,Paris,FRA
	65,Prague,CZE

now find who traveled to "USA" most often and export his USA visits into result.txt file
in the format "Name, City". And to make it a little spicy  

**You are not allowed to use Spark Dataframes**.

## Solution (scala) ##

1) Creating case classes for the data:

	case class Employee (
		id: String,
		name: String,
		role: String
	);

	case class Travel (
		employee_id: String,
		city: String,
		state: String
	);

	case class Result (
		employee_id: String,
		name: String,
		role: String,
		city: String
	);

I don't like to work with common tuples, I always prefer objects.

2) Parsing the rows from big data into mentioned objects.

	val fileEmployees = sc.textFile("/tests/employees.txt");
	val fileTravels = sc.textFile("/tests/e_travels.txt");

	val employeesObjects = fileEmployees.map(line=>Employee(line.split(",")(0), line.split(",")(1), line.split(",")(2)));
	val travelsObjects = fileTravels.map(line=>Travel(line.split(",")(0), line.split(",")(1), line.split(",")(2)));
	
	val ePairs = employeesObjects.map(employee=>(employee.id, employee));

ePairs will be used in the final code for printing the result.

3) Finding the employee who traveled to USA most often.

	val tPairs = travelsObjects.map(travel=>(travel.employee_id, travel)).filter(pair=>pair._2.state.equals("USA"));

	val rPairs = tPairs.map(travel=>(travel._1, 1));

	val traverel = rPairs.reduceByKey((a,b)=>a+b).map(pair=>(pair._2, pair._1)).sortByKey(false).map(pair=>(pair._2, pair._1)).take(1);

The idea is:

* tPairs contains now all employees who traveled  to USA. 
* rPairs contains tuples employee_id, 1 for finding who visited the USA most often.
* traveler contains employee who traveled to USA most often.

4) Printing the result

	val joined = ePairs.join(tPairs.filter(travel=>travel._1.equals(traverel(0)._1)));

	val result = joined.map(row=>(Result(row._1, row._2._1.name, row._2._1.role, row._2._2.city)));

	result.map(result=>result.name+","+result.city).saveAsTextFile("/tests/result1.txt");

* joined contains all travels to USA of the founded employee.
* result contains INNER JOIN mapped into Result object.

## Testing the result ##

* Put employees.txt and e_travels.txt files to HDFS Hadoop file system to /tests directory.
* run spark-shell -i test6.scala

You should see following output:

	[root@sandbox ~]# hdfs dfs -ls /tests/result1.txt
	Found 3 items
	-rw-r--r--   1 root hdfs          0 2018-01-19 08:27 /tests/result1.txt/_SUCCESS
	-rw-r--r--   1 root hdfs          0 2018-01-19 08:27 /tests/result1.txt/part-00000
	-rw-r--r--   1 root hdfs         25 2018-01-19 08:27 /tests/result1.txt/part-00001
	[root@sandbox ~]# hdfs dfs -cat /tests/result1.txt/part-00001
	John,NewYork
	John,Dallas

Best Regards

Tomas

